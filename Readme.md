# Technical Paper on General MVC Architecture and Spring framework

## MVC Architecture

The Model-View-Controller (MVC) is an architectural pattern that separates an application into three main logical components: the model, the view, and the controller. Each of these components are built to handle specific development aspects of an application. MVC is one of the most frequently used industry-standard web development framework to create scalable and extensible projects.

### Following are the components of MVC −

* Model

The Model component corresponds to all the data-related logic that the user works with. This can represent either the data that is being transferred between the View and Controller components or any other business logic-related data. For example, a Customer object will retrieve the customer information from the database, manipulate it and update it data back to the database or use it to render data.

* View

The View component is used for all the UI logic of the application. For example, the Customer view will include all the UI components such as text boxes, dropdowns, etc. that the final user interacts with.

* Controller

Controllers act as an interface between Model and View components to process all the business logic and incoming requests, manipulate data using the Model component and interact with the Views to render the final output. For example, the Customer controller will handle all the interactions and inputs from the Customer View and update the database using the Customer Model. The same controller will be used to view the Customer data.

## Spring Framework

### Introduction to Spring Framework -

Prior to the advent of Enterprise Java Beans (EJB), Java developers needed to use JavaBeans to create Web applications. Although JavaBeans helped in the development of user interface (UI) components, they were not able to provide services, such as transaction management and security, which were required for developing robust and secure enterprise applications. The advent of EJB was seen as a solution to this problem EJB extends the Java components, such as Web and enterprise components, and provides services that help in enterprise application development. However, developing an enterprise application with EJB was not easy, as the developer needed to perform various tasks, such as creating Home and Remote interfaces and implementing lifecycle callback methods which lead to the complexity of providing code for EJBs Due to this complication, developers started looking for an easier way to develop enterprise applications.

The Spring framework(which is commonly known as Spring) has emerged as a solution to all these complications This framework uses various new techniques such as Aspect-Oriented Programming (AOP), Plain Old Java Object (POJO), and dependency injection (DI), to develop enterprise applications, thereby removing the complexities involved while developing enterprise applications using EJB, Spring is an open source lightweight framework that allows Java EE 7 developers to build simple, reliable, and scalable enterprise applications. This framework mainly focuses on providing various ways to help you manage your business objects. It made the development of Web applications much easier as compared to classic Java frameworks and Application Programming Interfaces (APIs), such as Java database connectivity(JDBC), JavaServer Pages(JSP), and Java Servlet.

The Spring framework can be considered as a collection of sub-frameworks, also called layers, such as Spring AOP. Spring Object-Relational Mapping (Spring ORM). Spring Web Flow, and Spring Web MVC. It is a lightweight application framework used for developing enterprise applications. You can use any of these modules separately while constructing a Web application. The modules may also be grouped together to provide better functionalities in a Web application. Spring framework is loosely coupled because of dependency Injection.

### Evolution of Spring Framework

The Spring Framework was first released in 2004. After that there has been a significant major revision, such as Spring 2.0 provided XML namespaces and AspectJ support, Spring 2.5 provide annotation-driven configuration, Spring 3.0 provided a Java-based @Configuration model. The latest release of the spring framework is 4.0. it is released with the support for Java 8 and Java EE 7 technologies. Though you can still use Spring with an older version of java, the minimum requirement is restricted to Java SE 6. Spring 4.0 also supports Java EE 7 technologies, such as java message service (JMS) 2.0, java persistence API (JPA) 2.1, Bean validation 1.1, servlet 3.1, and JCache.

### Spring Framework Architecture

![Spring Framework](./Spring-Framework-Architecture.png)

The Spring framework consists of seven modules which are shown in the above Figure. These modules are Spring Core, Spring AOP, Spring Web MVC, Spring DAO, Spring ORM, Spring context, and Spring Web flow. These modules provide different platforms to develop different enterprise applications; for example, you can use Spring Web MVC module for developing MVC-based applications.

### Why Spring?

Java programs are complex and feature many heavyweight components. Heavyweight means the components are dependent on the underlying operating system (OS) for their appearance and properties.

Spring is considered to be a secure, low-cost and flexible framework. Spring improves coding efficiency and reduces overall application development time because it is lightweight -- efficient at utilizing system resources -- and has a lot of support.

Spring removes tedious configuration work so that developers can focus on writing business logic. Spring handles the infrastructure so developers can focus on the application.

### How Spring works

A web application (layered architecture) commonly includes three layers:

* Presentation/view layer (UI) - This is the outermost layer which handles the presentation of content and interaction with the user.
* Business logic layer - The central layer that deals with the logic of a program.
* Data access layer - The deep layer that deals with data retrieval from sources.

Each layer is dependent on the other for an application to work. In other words, the presentation layer talks to the business logic layer, which talks to the data access layer. Dependency is what each layer needs to perform its function. A typical application has thousands of classes and many dependencies.

Without a Spring Framework, application code tends to be tightly coupled (interdependent), which is not considered good coding practice. Loose coupling is ideal because loosely coupled components are independent, meaning changes in one will not affect the operation of others.

Spring’s core logic is dependency injection. Dependency injection is a programming pattern that allows developers to build more decoupled architectures. Dependency injection means that Spring understands the different Java annotations that a developer puts on top of classes. Spring knows that the developer wants to create an instance of a class and that Spring should manage it. Spring also understands the dependency and makes sure that all instances created have properly populated dependencies.

For the Spring Framework to instantiate objects and populate the dependencies, a programmer simply tells Spring which objects to manage and what the dependencies are for each class. A developer does so by using annotations like:

@component - Lets Spring know which classes to manage (create). Marks the beans (objects) as managed components, which means that Spring will autodetect these classes for dependency injection.

@autowired - Tells Spring how to handle the instantiation of the class (so it starts looking for that dependency among components/classes to find a match). This spares developers from wiring with code and allows Spring to find what needs to be injected where.

### References-

* [Techtarget website](https://www.techtarget.com/)
* [GeekforGeeks](https://www.geeksforgeeks.org/)
* [Tutorial Point](https://www.tutorialspoint.com/mvc_framework/mvc_framework_introduction.htm)
